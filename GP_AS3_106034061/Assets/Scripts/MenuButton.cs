﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButton : MonoBehaviour
{
    public AudioClip Music;
    public AudioSource audioSource;
    // void Start () {
    //     audioSource.PlayOneShot(Music);
    //     GameObject.DontDestroyOnLoad(audioSource);
    //     GameObject.DontDestroyOnLoad(Music);

    // }
    public void OnButtonClick(){
        Debug.Log("Start");
        SceneManager.LoadScene("Game1_Scene", LoadSceneMode.Single);
    }
    public void QuitClick(){
        Debug.Log("Quit");
        SceneManager.LoadScene("End_Scene", LoadSceneMode.Single);
    }
    public void ReplayClick(){
        Debug.Log("Replay");
        SceneManager.LoadScene("Start_Scene", LoadSceneMode.Single);
    }
}
