using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class BallController : MonoBehaviour {
	public Rigidbody2D rb;
	public Rigidbody2D hook;
	public float releaseTime = .15f;
	public float maxDragDistance = 2f;
	public GameObject nextBall;
	private bool isPressed = false;
    public bool released = false;
    public bool FirstHit = false;
    public AudioClip bird_launch;
    public AudioClip bird_hit;
    public AudioClip failed_level;
    public AudioSource audio;
    public int numberofhits;
    public Text ShootUI;
    public Text ScoreUI;
    public GameObject GC;

    private void Start()
    {
        audio = GetComponent<AudioSource>();
        GC = GameObject.Find("GameController");
        ShootUI = GameObject.Find("Canvas/Shoot").GetComponent<Text>();
        ScoreUI = GameObject.Find("Canvas/Score").GetComponent<Text>();

    }
    void Update ()
	{
		if (isPressed)
		{
			Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			if (Vector3.Distance(mousePos, hook.position) > maxDragDistance)
				rb.position = hook.position + (mousePos - hook.position).normalized * maxDragDistance;
			else
				rb.position = mousePos;
		}

	}
	void OnMouseDown (){
		isPressed = true;
		rb.isKinematic = true;
	}
	void OnMouseUp (){
		isPressed = false;
		rb.isKinematic = false;
		StartCoroutine(Release());
	}
    void OnCollisionEnter2D(Collision2D colInfo){
        if (colInfo.relativeVelocity.magnitude > 5){
            audio.PlayOneShot(bird_hit);
        }
    //     FirstHit = true;
    //     if(colInfo.gameObject.tag == "Enemy1"){
    //         GC.GetComponent<GameController>().score++;
    //         ScoreUI.gameObject.GetComponent<Text>().text = "Score:"+ GC.GetComponent<GameController>().score.ToString();
    //         Destroy(colInfo.gameObject);
    //     }
        if(colInfo.gameObject.tag == "Enemy2"){
            GC.GetComponent<GameController>().score -= 10;
            ScoreUI.gameObject.GetComponent<Text>().text = "Score:"+ GC.GetComponent<GameController>().score.ToString();
            Destroy(colInfo.gameObject);
        }
    //     else if(colInfo.gameObject.tag == "Coin"){
    //         GC.GetComponent<GameController>().score += 10;
    //         ScoreUI.gameObject.GetComponent<Text>().text = "Score:"+ GC.GetComponent<GameController>().score.ToString();
    //         Destroy(colInfo.gameObject);
    //     }
    //     else if(colInfo.gameObject.tag == "Star"){
    //         GC.GetComponent<GameController>().score +=100;
    //         ScoreUI.gameObject.GetComponent<Text>().text = "Score:"+ GC.GetComponent<GameController>().score.ToString();
    //         Destroy(colInfo.gameObject);
    //     }
    //     else if(colInfo.gameObject.tag == "Trap"){
    //         Destroy(colInfo.gameObject);
    //     }
    //     else if(colInfo.gameObject.tag == "Box"){
    //         Destroy(colInfo.gameObject);
    //     }
    //     else if(colInfo.gameObject.tag == "Hidden"){
    //         // Destroy(colInfo.gameObject);
    //     }
        if(colInfo.gameObject.tag == "Level"){
            SceneManager.LoadScene("Game"+"3"+"_Scene", LoadSceneMode.Single);
        }
    }
    void OnTriggerEnter2D(Collider2D colInfo){
        // if (colInfo.relativeVelocity.magnitude > 5){
            audio.PlayOneShot(bird_hit);
        // }
        FirstHit = true;
        if(colInfo.gameObject.tag == "Enemy1"){
            GC.GetComponent<GameController>().score++;
            ScoreUI.gameObject.GetComponent<Text>().text = "Score:"+ GC.GetComponent<GameController>().score.ToString();
            Destroy(colInfo.gameObject);
        }
        else if(colInfo.gameObject.tag == "Enemy2"){
            GC.GetComponent<GameController>().score -= 10;
            ScoreUI.gameObject.GetComponent<Text>().text = "Score:"+ GC.GetComponent<GameController>().score.ToString();
            Destroy(colInfo.gameObject);
        }
        else if(colInfo.gameObject.tag == "Coin"){
            GC.GetComponent<GameController>().score += 10;
            ScoreUI.gameObject.GetComponent<Text>().text = "Score:"+ GC.GetComponent<GameController>().score.ToString();
            Destroy(colInfo.gameObject);
        }
        else if(colInfo.gameObject.tag == "Star"){
            GC.GetComponent<GameController>().score +=100;
            ScoreUI.gameObject.GetComponent<Text>().text = "Score:"+ GC.GetComponent<GameController>().score.ToString();
            Destroy(colInfo.gameObject);
        }
        else if(colInfo.gameObject.tag == "Trap"){
            Destroy(colInfo.gameObject);
        }
        else if(colInfo.gameObject.tag == "Box"){
            Destroy(colInfo.gameObject);
        }
        else if(colInfo.gameObject.tag == "Hidden"){
            // Destroy(colInfo.gameObject);
        }
    }
    IEnumerator Release (){
		yield return new WaitForSeconds(releaseTime);
        released = true;
        audio.PlayOneShot(bird_launch);
        GetComponent<SpringJoint2D>().enabled = false;
		this.enabled = false;
		yield return new WaitForSeconds(1f);

		if (nextBall != null)
		{
			nextBall.SetActive(true);
            GC.GetComponent<GameController>().shoot++;
            ShootUI.gameObject.GetComponent<Text>().text = "Shoot:"+ GC.GetComponent<GameController>().shoot.ToString();

        } else
		{
            StartCoroutine(Reload());
        }
	    
	}
    
    IEnumerator Reload()
    {
        yield return new WaitForSeconds(2f);
        audio.PlayOneShot(failed_level);
        Debug.Log("Reloading");
        yield return new WaitForSeconds(5f);
        // GC.GetComponent<GameController>().level++;
        if(GC.GetComponent<GameController>().level == 3){
            if(GC.GetComponent<GameController>().score > 200){
                SceneManager.LoadScene("End_Scene", LoadSceneMode.Single);
            }else{
                SceneManager.LoadScene("Game"+GC.GetComponent<GameController>().level.ToString()+"_Scene", LoadSceneMode.Single);
            }
        }
        else if(GC.GetComponent<GameController>().level == 2){
            // GC.GetComponent<GameController>().level++;
            SceneManager.LoadScene("Game"+GC.GetComponent<GameController>().level.ToString()+"_Scene", LoadSceneMode.Single);
        }
        else if(GC.GetComponent<GameController>().level == 1){
            if(GC.GetComponent<GameController>().score > 100){
                GC.GetComponent<GameController>().level++;
            }
            SceneManager.LoadScene("Game"+GC.GetComponent<GameController>().level.ToString()+"_Scene", LoadSceneMode.Single);
        }
    }
}
