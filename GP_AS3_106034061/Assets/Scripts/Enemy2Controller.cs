﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2Controller : MonoBehaviour
{
    private Rigidbody2D rb2D;
    private Animator animator;
    private Vector2 dir;
    public GameObject GC;
    void Start()
    {
        GC = GameObject.Find("GameController");
        rb2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        dir = new Vector2(-5,0);
        InvokeRepeating("ChangeDireciton",1,1);
    }

    void Update()
    {
        // animator.SetFloat("x",Mathf.Abs(rb2D.velocity.x));
    }
    // Update is called once per frame
    private void ChangeDireciton()
    {
        dir *= -1;
        // gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x * -1 , gameObject.transform.localScale.y , gameObject.transform.localScale.z);
        rb2D.velocity = dir * 5;
    }
}
