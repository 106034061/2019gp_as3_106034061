﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//No Wall Grab, No Particle version
public class Movement : MonoBehaviour
{
    public Text ScoreUI;
    private float score;
    private Collision coll;
    private Animator animator;
    [HideInInspector]
    public Rigidbody2D rb;
    public float speed = 10;
    public float jumpForce = 50;


    private bool groundTouch;
    //private bool hasDashed;
    private bool hasDoubleJumped;




    // Start is called before the first frame update
    void Start()
    {
        coll = GetComponent<Collision>();
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        Vector2 dir = new Vector2(x, y);

        Walk(dir);
 
        // animator.SetFloat("x", Math.Abs(x));
        // animator.SetFloat("y", rb.velocity.y);
        // animator.SetBool("onGround", coll.onGround);


        if (Input.GetButtonDown("Jump"))
        {
            // animator.SetTrigger("Jump");

            if (coll.onGround)
                Jump(Vector2.up);
        }
        if(Input.GetKeyDown("r"))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }

        // if(x > 0)
        //     animator.gameObject.transform.localScale  = new Vector3(1,1,1);
        // if (x < 0)
        //     animator.gameObject.transform.localScale  = new Vector3(-1,1,1);
        
    }

    private void Walk(Vector2 dir)
    {

            rb.velocity = new Vector2(dir.x * speed, rb.velocity.y);
    }

    private void Jump(Vector2 dir)
    {
            rb.velocity = new Vector2(rb.velocity.x, 0);
            rb.velocity += dir * jumpForce;
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.CompareTag("Enemy"))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.CompareTag("Item"))
        {
            Destroy(col.gameObject);
            score++;
            ScoreUI.text = "Score : " + score;
        }
    }

}
