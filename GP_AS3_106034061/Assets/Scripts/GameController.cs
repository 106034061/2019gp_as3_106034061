﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public AudioSource audioSource;
    public int shoot = 1;
    public int level = 1 ;
    public int score = 0;
    public AudioClip[] bgList;
    // public AudioClip Music;
    void  Awake() {
        audioSource.clip = bgList[0];
        audioSource.Play();
        // GameObject.DontDestroyOnLoad(audioSource);
        // GameObject.DontDestroyOnLoad(bgList[1]);
        // GameObject.DontDestroyOnLoad(bgList[0]);
        // GameObject.DontDestroyOnLoad(this.gameObject);

    }
    void Update(){
        if(Input.GetKeyDown("r")){
            SceneManager.LoadScene("Game"+level.ToString()+"_Scene", LoadSceneMode.Single);
        }
    }
    public void OnToggleChange(bool t){
        audioSource.mute = t;
        cancelFocus();
    }

    public void OnDropdownChange(int index){
        audioSource.clip = bgList[index];
        audioSource.Play();
        cancelFocus();
    }

    public void OnSliderChange(float value){
        audioSource.volume = value;
    }

    void cancelFocus(){
        EventSystem.current.SetSelectedGameObject(null);
    }
}
